const tipoInput = document.querySelector('#input-tipo');
const montoInput = document.querySelector('#input-monto');
const guardarBtn = document.querySelector('#btn-guardar');

const gastosList = document.querySelector('#list-gastos');
const totalOutput = document.querySelector('#output');

let total = 0;

function clear(){
    tipoInput.value = '';
    montoInput.value = '';
}

guardarBtn.addEventListener('click', ()=>{

    const tipo = tipoInput.value;
    const monto = montoInput.value;

    //alert(1);

    if(tipo.trim().length > 0 && monto.trim().length > 0 && monto >0){

        //agregamos el item de la lista
        const newItem = document.createElement('ion-item');
        newItem.textContent = tipo + ': $' + monto;
        gastosList.appendChild(newItem);

        //totallizamos los montos de los gastos
        total += +monto;
        totalOutput.textContent = total;

        //limpiamos los inputs
        clear();
    }
    else{
        console.error('Valores invalidos ! ');
        
        alertController.create({
            message: 'Llene los compos correctamente',
            header: 'Valores invalidos',
            buttons: ['Ok']
        }).then(alertElement => {
            alertElement.present();
        });
    }
});